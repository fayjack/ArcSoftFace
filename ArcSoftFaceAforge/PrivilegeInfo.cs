﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArcSoftFaceAforge
{
    class PrivilegeInfo
    {
        public long id { get; set; }
        public string personSN { get; set; }
        public string personName { get; set; }
        public string validTo { get; set; }
        public string type { get; set; }
    }
}
