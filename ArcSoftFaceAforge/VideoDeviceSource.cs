﻿using System;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using AForge.Video;
using AForge.Video.DirectShow;
using Ini.Net;
using Newtonsoft.Json;

namespace ArcSoftFaceAforge
{
    class VideoDeviceSource
    {
        private readonly object sync = new object();
        private IVideoSource _captureDevice;
        private IniFile config = new IniFile("相机配置.ini");
        private Bitmap currentFrame;
        public delegate void FPS(object sender, Bitmap bitmap);
        public event FPS _fps;

        
        private bool requestStop = false;

        public void Start() {
            if(_captureDevice == null)
            {
                requestStop = false;
                //使用USB摄像机识别
                _captureDevice = GetCaptureDevice();
                //使用本地Resources目录下的720p.avi文件识别测试
                //_captureDevice = GetFileVideoSource();
                _captureDevice.NewFrame += _captureDevice_NewFrame; ;
                _captureDevice.Start();
            }
        }


        public void Stop()
        {
            if (_captureDevice == null) return;
            requestStop = true;
            _captureDevice.NewFrame -= _captureDevice_NewFrame;
            _captureDevice.WaitForStop();
        }

        private void _captureDevice_NewFrame(object sender, NewFrameEventArgs eventArgs)
        {
            if (requestStop) return;
            Bitmap newFrame = (Bitmap)eventArgs.Frame.Clone();
            //如果使用视频文件请注释下面的这行代码，表示不对图像进行水平翻转
            newFrame.RotateFlip(RotateFlipType.Rotate180FlipY);
            lock (sync)
            {
                if (currentFrame != null)
                {
                    currentFrame.Dispose();
                    currentFrame = null;
                }
                currentFrame = newFrame;
                _fps.Invoke(sender, newFrame);
            }
        }

        public Bitmap GetCurrentFrame()
        {
            if (requestStop) return null;
            lock (sync)
            {
                return (currentFrame == null || currentFrame.Width == 0 || currentFrame.Height == 0) ? null : AForge.Imaging.Image.Clone(currentFrame);
            }
        }



        private VideoCaptureDevice GetCaptureDevice()
        {
            if (string.IsNullOrWhiteSpace(config.ReadString("相机配置", "MonikerString")))
            {
                ConfigCamera();
            }
            var MonikerString = config.ReadString("相机配置", "MonikerString");
            var FrameSize = config.ReadString("相机配置", "FrameSize");
            var videoCaptureDevice = new VideoCaptureDevice(MonikerString);
            var capabilities = videoCaptureDevice.VideoCapabilities;
            foreach (var item in capabilities)
            {
                if (item.FrameSize.ToString().Equals(FrameSize))
                {
                    videoCaptureDevice.VideoResolution = item;
                    break;
                }
            }
            return videoCaptureDevice;
        }

        private FileVideoSource GetFileVideoSource()
        {
            return new FileVideoSource(Path.Combine("Resources","720p.avi"));
        }

        public void ContinueStart() {
            if(requestStop == false && _captureDevice != null && !_captureDevice.IsRunning)
            {
                _captureDevice.Start();
            }
        }

        public void ConfigCamera()
        {
            var MonikerString = config.ReadString("相机配置", "MonikerString");
            var FrameSize = config.ReadString("相机配置", "FrameSize");

            VideoCaptureDeviceForm deviceForm = new VideoCaptureDeviceForm();
            deviceForm.TopMost = true;
            if (!string.IsNullOrEmpty(FrameSize) && _captureDevice != null && _captureDevice.GetType() == typeof(VideoCaptureDevice))
            {
                VideoCaptureDevice vcd = (VideoCaptureDevice)_captureDevice;
                VideoCapabilities[] vcs = vcd.VideoCapabilities;
                foreach(var item in vcs)
                {
                    if (item.FrameSize.ToString().Equals(FrameSize))
                    {
                        deviceForm.CaptureSize = item.FrameSize;
                        break;
                    }
                }
            }
            
            DialogResult dr = deviceForm.ShowDialog();
            //如果未加载到相机,则系统退出
            if(deviceForm.VideoDevice == null)
            {
                MessageBox.Show("未加载到相机,请退出软件后检查相机是否正常");
                Environment.Exit(0);
            }

            //如果从未设置过，第一次又点了取消，则默认使用第一选项
            if((string.IsNullOrEmpty(MonikerString) || string.IsNullOrEmpty(FrameSize)) && DialogResult.Cancel == dr)
            {
                MessageBox.Show("未配置相机,软件将退出");
                Environment.Exit(0);
            }
            //如果确认当前选项，则使用当前选中配置
            if(DialogResult.OK == dr)
            {
                config.WriteString("相机配置", "MonikerString", deviceForm.VideoDeviceMoniker);
                config.WriteString("相机配置", "FrameSize", deviceForm.VideoDevice.VideoResolution.FrameSize.ToString());
            }
        }

        public void PaintToPictureBox(PictureBox pictureBox1, Graphics g)
        {
            if (requestStop) return;
            using(Bitmap bitmap = GetCurrentFrame())
            {
                if (currentFrame == null)
                {
                    return;
                }

                if (pictureBox1.Image == null)
                {
                    pictureBox1.Image = new Bitmap(pictureBox1.Width, pictureBox1.Height);
                    return;
                }
                g.DrawImage(bitmap, (pictureBox1.Width - bitmap.Width) / 2, (pictureBox1.Height - bitmap.Height) / 2);
            }
        }

    }
}
