﻿using Ini.Net;
using Stepon.FaceRecognization;
using Stepon.FaceRecognization.Common;
using Stepon.FaceRecognization.Detection;
using Stepon.FaceRecognization.Extensions;
using Stepon.FaceRecognization.Recognization;
using Stepon.FaceRecognization.Tracking;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Threading;

namespace ArcSoftFaceAforge
{
    public class ArcFaceApi
    {
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(typeof(ArcFaceApi));
        private const string Sn = "SN.ini";
        public const string Repository = "人脸库";
        private readonly FaceTracking _traking;
        private readonly FaceDetection _detection;
        private readonly FaceRecognize _recognize;
        private readonly FaceProcessor _processor;

        private readonly Stopwatch _trackingRectangleStopwatch = Stopwatch.StartNew();
        public Rectangle TrackingRectangle = Rectangle.Empty;

        private readonly Dictionary<string, byte[]> _cache = new Dictionary<string, byte[]>();
        private readonly ReaderWriterLockSlim _cacheLock = new ReaderWriterLockSlim();
        private readonly Stopwatch _faceStopwatch = Stopwatch.StartNew();

        private bool _showTrackingRectangle = false;
        public int FaceSize = 0;

        public ArcFaceApi()
        {
            try
            {
                var iniFile = new IniFile(Sn);
                if (!File.Exists(Sn))
                {
                    iniFile.WriteString("注册码", "AppId", "pdhEENp6Ph69tmrMaF6tUPHT3piYN8r1jAYa1Mi4VA5");
                    iniFile.WriteString("注册码", "FtKey", "BVAXc57zmq69RxHRvowrDpfMxjEYSV6yZZ7nygH99Rwt");
                    iniFile.WriteString("注册码", "FdKey", "BVAXc57zmq69RxHRvowrDpfV88VkEh9UXBEoH3kG266K");
                    iniFile.WriteString("注册码", "FrKey", "BVAXc57zmq69RxHRvowrDpfymjYSGh7MGBa9Kn8NiW54");
                }

                var appId = iniFile.ReadString("注册码", "AppId");
                var ftKey = iniFile.ReadString("注册码", "FtKey");
                var fdKey = iniFile.ReadString("注册码", "FdKey");
                var frKey = iniFile.ReadString("注册码", "FrKey");

                _traking = LocatorFactory.GetTrackingLocator(appId, ftKey) as FaceTracking;
                _detection = LocatorFactory.GetDetectionLocator(appId, fdKey) as FaceDetection;
                _recognize = new FaceRecognize(appId, frKey);
                var traking = LocatorFactory.GetTrackingLocator(appId, ftKey) as FaceTracking;
                _processor = new FaceProcessor(traking, _recognize);
                Log.Info("初始化人脸识别动态库成功");
            }
            catch (Exception e)
            {
                Log.Error("初始化人脸识别动态库失败",e);
            }
        }

        public bool IsFaceEmpty()
        {
            return TrackingRectangle.IsEmpty || TrackingRectangle.Width < 80;
        }

        /**
         * 人脸追踪，每100ms追踪一次
         * 追踪后将最大的人脸矩形保存到 _trackingRectangle 中
         **/
        public void FaceTracking(Bitmap bitmap)
        {
            if(_trackingRectangleStopwatch.ElapsedMilliseconds < 100)
            {
                return;
            }
            _trackingRectangleStopwatch.Restart();
            try
            {
                LocateResult locateResult;
                _traking.Detect(bitmap, out locateResult);
                using (locateResult)
                {
                    if (locateResult == null || locateResult.FaceCount == 0)
                    {
                        TrackingRectangle = Rectangle.Empty;
                        return;
                    }
                    var orderBy = locateResult.Faces.Select(s=>s.ToRectangle()).OrderBy(o => o.Width).ThenBy(o=>o.Left).ToList();
                    foreach (var item in orderBy) {
                        DrawFaceRectangle(bitmap, item);
                    }
                    TrackingRectangle = orderBy.Last();
                    Log.DebugFormat("追踪到人脸:{0} 最大人脸:{1}", orderBy.Select(s => s.ToString()).Aggregate((partialPhrase, word) => $"{partialPhrase} {word}"), TrackingRectangle);
                    Log.DebugFormat("人脸追踪矩形:{0} 耗时:{1}", TrackingRectangle, _trackingRectangleStopwatch.ElapsedMilliseconds);
                }
            }
            catch (Exception e)
            {
                Log.Error("人脸追踪异常", e);
            }
        }

        /**
         * 人脸矩形绘制
         **/
        private void DrawFaceRectangle(Bitmap bitmap, Rectangle rect)
        {
            if (rect.IsEmpty) { return; }
            using (Graphics g = Graphics.FromImage(bitmap))
            {
                DrawFaceRectangle(g, rect);
            }
        }

        /**
         * 人脸矩形绘制
         **/
        private void DrawFaceRectangle(Graphics graphics, Rectangle rect)
        {
            if (rect.IsEmpty || !_showTrackingRectangle) { return; }
            using (var pen = new Pen(Color.White, 5.0f))
            {
                var length = rect.Width / 4;
                graphics.DrawLines(pen, new[] { new Point(rect.X, rect.Y + length), new Point(rect.X, rect.Y), new Point(rect.X + length, rect.Y) });

                graphics.DrawLines(pen,
                    new[]
                    {
                           new Point(rect.X + rect.Width, rect.Y + length), new Point(rect.X + rect.Width, rect.Y), new Point(rect.X + rect.Width - length, rect.Y)
                    });
                graphics.DrawLines(pen,
                    new[]
                    {
                           new Point(rect.X, rect.Y + rect.Height - length), new Point(rect.X, rect.Y + rect.Height), new Point(rect.X + length, rect.Y + rect.Height)
                    });

                graphics.DrawLines(pen,
                    new[]
                    {
                            new Point(rect.X + rect.Width - length, rect.Y + rect.Height),new Point(rect.X + rect.Width, rect.Y + rect.Height), new Point(rect.X + rect.Width, rect.Y + rect.Height - length)
                    });
                graphics.DrawString(rect.Width.ToString(), new Font(FontFamily.GenericMonospace, 25.0f), Brushes.White, new Point(rect.X + rect.Width, rect.Y));
                Log.DebugFormat("绘制人脸矩形:{0}", rect);
            }

        }

        /**
         * 加载本地人脸库
         **/
        public void LoadRepository()
        {
            Log.InfoFormat("准备加载本地人脸库:{0}", Repository);
            if (!Directory.Exists(Repository))
            {
                Log.InfoFormat("创建本地对比库:{0}", Repository);
                Directory.CreateDirectory(Repository);
                return;
            }

            
            try
            {
                _cacheLock.EnterWriteLock();
                _cache.Clear();
                Log.Info("清理1:N缓存区所有数据");
            }
            finally
            {
                _cacheLock.ExitWriteLock();
            }

            Log.InfoFormat("加载本地人脸对比库:{0}", Repository);
            var files = Directory.GetFiles(Repository).Where(w => w.EndsWith("jpg"));
            foreach (var file in files)
            {
                try
                {
                    AddFileToCache(file);
                }
                catch (Exception e)
                {
                    Log.Error("添加人脸到对比库异常", e);
                }
            }
        }

        public void AddFileToCache(string file)
        {
            var fi = new FileInfo(file);
            var name = fi.Name.Replace(fi.Extension,"");
            //var data = Path.Combine(_repository, $"{name}.dat");

            //if (File.Exists(data))
            //{
            //    log.DebugFormat("己存在人脸模板：{0}", data);
            //    AddToCache(name, File.ReadAllBytes(data));
            //    return;
            //}

            //log.DebugFormat("创建人脸模板：{0}", data);
            var faceModel = ToFaceModel(file);
            AddToCache(name, faceModel.Data);
            //File.WriteAllBytes(data, faceModel);
        }


        public void DeleteFromRepository(string destFileName)
        {
            var fi = new FileInfo(destFileName);
            var name = fi.Name.Replace(fi.Extension, "");
            var data = Path.Combine(Repository, $"{name}.dat");
            var jpg = Path.Combine(Repository, $"{name}.jpg");

            if (File.Exists(data))
            {
                File.Delete(data);
            }

            if (File.Exists(jpg))
            {
                File.Delete(jpg);
            }
            else
            {
                string[] files = Directory.GetFiles(Repository);
                string delete = "";
                foreach (string file in files)
                {
                    FileInfo f = new FileInfo(file);
                    string[] splits = f.Name.Split('_');
                    string[] splits2 = fi.Name.Split('_');
                    if (splits.Length > 0 && splits2.Length > 0 && splits[0].Equals(splits2[0])) {
                        delete = file;
                    }
                }
                if (!string.IsNullOrEmpty(delete))
                {
                    File.Delete(delete);
                    FileInfo di = new FileInfo(delete);
                    DeleteFromCache(di.Name.Replace(di.Extension,""));
                    return;
                }
            }

            DeleteFromCache(name);
        }

        private void DeleteFromCache(string name) {
            if (!_cache.ContainsKey(name)) return;

            try
            {
                _cacheLock.EnterWriteLock();
                _cache.Remove(name);
                FaceSize = _cache.Count();
            }
            finally
            {
                _cacheLock.ExitWriteLock();
            }
        }


        /**
         * 转换人脸图片为人脸模板
         **/
        private FaceModel ToFaceModel(string file)
        {
            using (var bitmap = new Bitmap(file))
            {
                LocateResult locate;
                var code = _detection.Detect(bitmap, out locate);
                if (code == ErrorCode.Ok && locate.HasFace && locate.FaceCount == 1)
                {
                    using (var feature = _recognize.ExtractFeature(locate.OffInput, locate.Faces[0], locate.FacesOrient[0]))
                    {
                        return feature.FeatureData;
                    }
                }
                locate.Dispose();
            }
            return null;
        }

        /**
         * 加载人脸模板到1:N高速对比库
         **/
        private void AddToCache(string name, byte[] faceModel)
        {
            if (faceModel == null)
            {
                return;
            }
            try
            {
                _cacheLock.EnterWriteLock();
                if (_cache.ContainsKey(name))
                {
                    _cache.Remove(name);
                }
                _cache.Add(name, faceModel);
                FaceSize = _cache.Count();
                Log.InfoFormat("添加人脸到对比库:{0}", name);
            }
            finally
            {
                _cacheLock.ExitWriteLock();
            }
        }

        internal void Clean()
        {
            try
            {
                _cacheLock.EnterWriteLock();
                _cache.Clear();
                FaceSize = _cache.Count();
                Log.InfoFormat("清空人脸对比库:{0}", Repository);
            }
            finally
            {
                _cacheLock.ExitWriteLock();
            }
        }

        /**
         * 1:N 人脸对比
         **/
        public string Match(Bitmap bitmap)
        {
            if (bitmap == null) { return ""; }
            _faceStopwatch.Restart();
            try
            {
                var features = _processor.LocateExtract(bitmap);
                if (features != null && features.Length >= 1)
                {
                    var max = features.OrderBy(o => o.Rect.ToRectangle().Width).ThenBy(t=>t.Rect.ToRectangle().Left).Last();
                    var name = Match(max);
                    return name;
                }
            }
            catch (Exception e)
            {
                Log.Error("人脸识别异常", e);
            }
            finally
            {
                Log.DebugFormat("人脸对比耗时:{0}", _faceStopwatch.ElapsedMilliseconds);
            }
            return "";
        }

        private string Match(Feature feature)
        {
            if (feature == null) { return ""; }
            
            try
            {
                bool enter = _cacheLock.TryEnterReadLock(TimeSpan.FromMilliseconds(50));
                if (!enter) return "";
                foreach (var single in _cache)
                {
                    var sim = _processor.Match(feature.FeatureData, single.Value); //此方法默认保留采集到的特征（非托管内存），并自动释放被比较（特征库）的特征数据，所以无需担心内存泄露
                    if (sim > 0.7)
                    {
                        return single.Key;
                    }
                }
            }
            finally
            {
                _cacheLock.ExitReadLock();
            }
            return "";
        }


        public void TiggerTracking()
        {
            _showTrackingRectangle = !_showTrackingRectangle;
        }


        public Bitmap Zoom(Bitmap bitmap, int new_Width, int new_Height)
        {
            ///--新建一个bitmap对象
            Bitmap newBitmap = new Bitmap(new_Width, new_Width);
            Graphics newG = Graphics.FromImage(newBitmap);
            ///--插入算法的质量
            newG.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.Low;
            ///--重绘
            newG.DrawImage(bitmap, new Rectangle(0, 0, new_Width, new_Width), new Rectangle(0, 0, bitmap.Width, bitmap.Height), GraphicsUnit.Pixel);
            ///--释放绘图工具
            newG.Dispose();
            ///---返回新结果
            return newBitmap;
        }


    }
}
