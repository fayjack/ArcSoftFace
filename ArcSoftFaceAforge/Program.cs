﻿using System;
using System.Windows.Forms;

namespace ArcSoftFaceAforge
{
    static class Program
    {
        /// <summary>
        /// 应用程序的主入口点。
        /// </summary>
        [STAThread]
        static void Main()
        {
            var log = log4net.LogManager.GetLogger(typeof(Program)); ;
            try
            {
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                Application.Run(new Main());
            }
            catch (Exception e)
            {
                log.Error("启动失败", e);
            }
        }
    }
}
