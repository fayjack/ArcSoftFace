﻿using System;
using System.Drawing;
using System.IO;
using System.Windows.Forms;

namespace ArcSoftFaceAforge
{
    public partial class AddFace : Form
    {
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(typeof(ArcFaceApi));

        private ArcFaceApi _faces;
        private Bitmap _bitmap;

        public AddFace()
        {
            InitializeComponent();
        }

        public void Init(Bitmap bitmap, ArcFaceApi faces)
        {
            _bitmap = bitmap;
            _faces = faces;
        }

        private void AddFace_Load(object sender, EventArgs e)
        {
            pictureBox1.Image = _bitmap;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(textBox1.Text))
            {
                MessageBox.Show(@"用户编号不可为空");
                return;
            }

            if (string.IsNullOrEmpty(textBox2.Text))
            {
                MessageBox.Show(@"用户姓名不可为空");
                return;
            }
            try
            {
                var fileName = ArcFaceApi.Repository + "/" + textBox1.Text.Trim() + "_" + textBox2.Text.Trim() + ".jpg";
                var fi = new FileInfo(fileName);
                if (fi.Exists) fi.Delete();
                pictureBox1.Image.Save(fileName);
                _faces.AddFileToCache(fileName);
            }
            catch (Exception ex)
            {
                MessageBox.Show("添加失败", @"错误", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Close();
            }
        }

        private void AddFace_FormClosed(object sender, FormClosedEventArgs e)
        {
            try
            {
                _bitmap?.Dispose();
            }
            catch (Exception ex)
            {
                Log.Error("关闭添加人脸对话框异常", ex);
            }
        }
    }
}