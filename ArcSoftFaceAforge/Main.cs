﻿using AutoUpdaterDotNET;
using System;
using System.Drawing;
using System.IO;
using System.Media;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ArcSoftFaceAforge
{
    public partial class Main : Form
    {
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(typeof(Main));
        private readonly CancellationTokenSource _cancellationTokenSource = new CancellationTokenSource();
        private readonly VideoDeviceSource _videoDeviceSource = new VideoDeviceSource();
        private ServiceConfig config = new ServiceConfig();
        private readonly ArcFaceApi _arcFaceApi;
        private string _name;
        private const string RecordPath = "识别记录";

        private FaceIdentifierResult _lastIdentifierResult;
        private readonly SoundPlayer _souPlayer;
        private readonly SoundPlayer _souPlayer2;
        private readonly FaceRepositoryManage _faceRepositoryManage;

        private bool _skipFps;

        private long _errorTimes;
        private long _emptyTime;

        private string _message = "";
        private bool isAddImage;

        public Main()
        {
            InitializeComponent();

            if (!string.IsNullOrWhiteSpace(config.UpdateUrl()))
            {
                Log.InfoFormat("启动检查更新:{0}",config.UpdateUrl());
                AutoUpdater.Start(config.UpdateUrl());
            }
            else {
                Log.Warn("未启用客户版本更新设置");
            }

            try
            {
                serialPort1.Open();
            }
            catch (Exception e)
            {
                Log.Error("打开串口1失败", e);
                MessageBox.Show("打开COM1失败,将无法进行开闸操作");
            }

            _arcFaceApi = new ArcFaceApi();
            

            _faceRepositoryManage = new FaceRepositoryManage(_arcFaceApi);
            

            var successFile = Path.Combine("Resources", "success.wav");
            if (File.Exists(successFile))
            {
                _souPlayer = new SoundPlayer(successFile);
                Log.InfoFormat("加载success音效文件成功:{0}", successFile);
            }

            var errorFile = Path.Combine("Resources", "error.wav");
            if (File.Exists(errorFile))
            {
                _souPlayer2 = new SoundPlayer(errorFile);
                Log.InfoFormat("加载error音效文件成功:{0}", errorFile);
            }


            if (!Directory.Exists(RecordPath))
            {
                Directory.CreateDirectory(RecordPath);
                Log.InfoFormat("创建识别记录文件夹:{0}", RecordPath);
            }
            Task.Factory.StartNew(FaceIdentifier, _cancellationTokenSource.Token);
        }

        private void FaceIdentifier()
        {
            Task.Delay(3000).Wait();
            try
            {
                _arcFaceApi.LoadRepository();
                _faceRepositoryManage.StartService();
            }
            catch (Exception ex)
            {
                Log.Error("加载本地人脸库失败", ex);
            }
            while (!_cancellationTokenSource.IsCancellationRequested)
            {
                try
                {
                    if (_arcFaceApi.IsFaceEmpty() || isAddImage) continue;

                    WakeupScreen();

                    using (var bitmap = _videoDeviceSource.GetCurrentFrame())
                    {
                        _name = _arcFaceApi.Match(bitmap);
                        Log.DebugFormat("识别人脸:{0}", _name);
                        SaveIdentifierRecord(_name, bitmap);
                    }
                }
                catch (Exception ex)
                {
                    Log.Error("识别人脸异常", ex);
                }
                finally
                {
                    UpdateMessage();
                    Thread.Sleep(200);
                }

            }
        }

        private void WakeupScreen() {
            try
            {
                MouseFlag.MouseLefClickEvent(0, 0, 0);
            }
            catch (Exception e)
            {
                Log.Warn("唤醒屏幕失败", e);
            }
        }

        private void UpdateMessage() {
            if (isAddImage)
            {
                return;
            }
            _emptyTime = _arcFaceApi.IsFaceEmpty() ? ++_emptyTime : 0;
            _errorTimes = _arcFaceApi.IsFaceEmpty() || !string.IsNullOrEmpty(_name) ? 0 : ++_errorTimes;

            if (_emptyTime > 5)
            {
                _message = @"欢迎使用人脸识别认证系统";
            }
            else if (_errorTimes > 5)
            {
                _message = @"认证失败";
                PlayError();
                Thread.Sleep(1000);
            }
            else if (_lastIdentifierResult != null && _emptyTime == 0 && _errorTimes == 0)
            {
                _message = _lastIdentifierResult.Name + @"认证成功";
            }
            else
            {
                _message = "正在认证,请稍后";
            }
        }

        private void SaveIdentifierRecord(string name,Bitmap img)
        {
            
            using(var bitmap = _arcFaceApi.Zoom(img,img.Width / 4,img.Height / 4))
            {
                try
                {
                    if (string.IsNullOrEmpty(name)) return;

                    var splits = _name.Split('_');
                    if (splits.Length < 2)
                    {
                        Log.Error("人脸库文件命名不正确，至少包含两个下划线来分隔信息");
                        return;
                    }

                    //同一编号的用户，在3秒之内不再保存开门记录
                    if (_lastIdentifierResult != null && _lastIdentifierResult.Identifier.Equals(splits[0]) && DateTime.Now.Subtract(_lastIdentifierResult.Time).Seconds < 2)
                    {
                        Log.DebugFormat("用户:{0} 在 {1} 己进行人脸识别,不再保存记录", _lastIdentifierResult.Name, _lastIdentifierResult.Time.ToString("yyyyMMddHHmmss"));
                        return;
                    }

                    openDoor();
                    PlaySuccess();
                    //保存人脸记录在文件
                    var fileName = _name + "_" + DateTime.Now.ToString("yyyyMMddHHmmss");
                    var file = Path.Combine(RecordPath, $"{fileName}.jpg");
                    _lastIdentifierResult = new FaceIdentifierResult(splits[0], splits[1], DateTime.Now, file);
                    bitmap.Save(file);
                    Log.InfoFormat("保存人脸记录成功", file);
                }
                catch (Exception e)
                {
                    Log.Error("保存人脸记录异常", e);
                }
            }
        }

        private void openDoor()
        {
            if (serialPort1.IsOpen)
            {
                try
                {
                    var hex = config.getOpenDoorCode();
                    if(string.IsNullOrEmpty(hex))
                    {
                        return;
                    }

                    string[] splits = hex.Split(' ');

                    if(splits.Length == 0)
                    {
                        return;
                    }

                    var inputByteArray = new byte[splits.Length];
                    for (var x = 0; x < inputByteArray.Length; x++)
                    {
                        var i = Convert.ToInt32(splits[x], 16);
                        inputByteArray[x] = (byte)i;
                    }
                    serialPort1.Write(inputByteArray, 0, inputByteArray.Length);
                }
                catch (Exception e)
                {
                    Log.Error("开闸失败",e);
                    throw;
                }
            }
        }

        private void Main_Load(object sender, EventArgs e)
        {
            try
            {
                _videoDeviceSource._fps += VideoDeviceSource_NewFrame;
                _videoDeviceSource.Start();
            }
            catch (Exception ex)
            {
                Log.Error("系统启动失败", ex);
            }
        }

        private void VideoDeviceSource_NewFrame(object sender, Bitmap image)
        {
            try
            {
                //_skipFps = !_skipFps;
                //if (_skipFps)
                //{
                //    return;
                //}

                if (isAddImage)
                {
                    return;
                }
                _arcFaceApi.FaceTracking(image);
                pictureBox1.Invalidate();
            }
            catch (Exception e)
            {
                Log.Error("采集相机异常", e);
            }
        }

        private void Main_FormClosed(object sender, FormClosedEventArgs e)
        {
            Log.Info("系统退出");
            _cancellationTokenSource.Cancel();
            _videoDeviceSource.Stop();
            if(_faceRepositoryManage != null)
            {
                _faceRepositoryManage.StopService();
            }

            if(_souPlayer != null)
            {
                _souPlayer.Dispose();
            }
        }

        public void PlaySuccess()
        {
            if(_souPlayer != null)
            {
                try
                {
                    _souPlayer.Play();
                }
                catch (Exception e)
                {
                    Log.Error("播放认证成功音效异常",e);
                }
            }
        }


        public void PlayError()
        {
            if (_souPlayer2 != null)
            {
                try
                {
                    _souPlayer2.Play();
                }
                catch (Exception e)
                {
                    Log.Error("播放认证失败音效异常", e);
                }
            }
        }

        private void UpdateTime_Tick(object sender, EventArgs e)
        {
            label3.Text = DateTime.Now.ToString("yyyy年MM月dd日HH时mm分ss秒") + "  (" + (_arcFaceApi == null ? 0 : _arcFaceApi.FaceSize) + ")";
            _videoDeviceSource.ContinueStart();
            label1.Text = _message;
        }

        private void PictureBox1_Paint(object sender, PaintEventArgs e)
        {
            _videoDeviceSource.PaintToPictureBox(pictureBox1,e.Graphics);
        }

        private void toolStripMenuItem1_Click(object sender, EventArgs e)
        {
            try
            {
                isAddImage = true;
                AddFace addFace = new AddFace();
                addFace.Init(_videoDeviceSource.GetCurrentFrame(), _arcFaceApi);
                addFace.ShowDialog();
            }
            finally
            {
                isAddImage = false;
            }
        }

        private void 分辨率ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            _videoDeviceSource.ConfigCamera();
            MessageBox.Show("重新设置了分辨率,请重新启动");
        }

        private void 退出ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Environment.Exit(0);
        }

        private void 显示方框ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            _arcFaceApi.TiggerTracking();
        }

        private void 重新加载人脸库ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                isAddImage = true;
                _arcFaceApi.Clean();
                _arcFaceApi.LoadRepository();
            }
            catch (Exception ex)
            {
                Log.Error("重新加载人脸库失败", ex);
            }
            finally
            {
                isAddImage = false;
            }
        }
    }
}
